﻿// <copyright file="XML.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Xml;

    /// <summary>
    /// Read/write XMLs.
    /// </summary>
    public static class XML
    {
        /// <summary>
        /// Global leadboard object.
        /// </summary>
        public static List<LeadboardItem> leadboard = new List<LeadboardItem>();

        /// <summary>
        /// Global gameItems object for save/load game.
        /// </summary>
        public static List<string> GameItems = new List<string>();

        /// <summary>
        /// Global etc object for save/load game.
        /// </summary>
        public static string Etc;

        /// <summary>
        /// Read leadboard from xml.
        /// </summary>
        /// <returns>List of leadboard items.</returns>
        public static List<LeadboardItem> ReadLeadboard()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("leadboard.xml");

            foreach (XmlNode node in doc.DocumentElement)
            {
                LeadboardItem currentItem = new LeadboardItem();
                foreach (XmlNode child in node.ChildNodes)
                {
                    if (child.Name == "name")
                    {
                        currentItem.Name = child.InnerText;
                    }

                    if (child.Name == "score")
                    {
                        currentItem.Score = Convert.ToInt32(child.InnerText);
                    }
                }

                leadboard.Add(currentItem);
            }

            return leadboard;
        }

        /// <summary>
        /// Write leadboard to xml.
        /// </summary>
        /// <param name="leadboardItem">Leadboard item to write.</param>
        public static void WriteLeadboard(LeadboardItem leadboardItem)
        {
            leadboard.Add(leadboardItem);
            XmlWriter xmlWriter = XmlWriter.Create("leadboard.xml");
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("leadboard");

            foreach (var item in leadboard)
            {
                xmlWriter.WriteStartElement("leadboardItem");
                xmlWriter.WriteStartElement("name");
                xmlWriter.WriteString(item.Name);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("score");
                xmlWriter.WriteString(item.Score.ToString());
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
        }

        /// <summary>
        /// Save game state to xml.
        /// </summary>
        /// <param name="fileName">file name.</param>
        /// <param name="etc">string to save.</param>
        /// <param name="gameItems">items to save.</param>
        public static void SaveGame(string fileName, string etc, List<string> gameItems)
        {
            XmlWriter xmlWriter = XmlWriter.Create(fileName);
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("root");

            xmlWriter.WriteStartElement("etc");
            xmlWriter.WriteString(etc);
            xmlWriter.WriteEndElement();

            foreach (var item in gameItems)
            {
                xmlWriter.WriteStartElement("gameItem");
                xmlWriter.WriteString(item);
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
        }

        /// <summary>
        /// Load saved game.
        /// </summary>
        /// <param name="fileName">file name.</param>
        public static void LoadGame(string fileName)
        {
            GameItems = new List<string>();
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);

            foreach (XmlNode node in doc.DocumentElement)
            {
                if (node.Name == "etc")
                {
                    Etc = node.InnerText;
                }

                if (node.Name == "gameItem")
                {
                    GameItems.Add(node.InnerText);
                }
            }
        }
    }
}
