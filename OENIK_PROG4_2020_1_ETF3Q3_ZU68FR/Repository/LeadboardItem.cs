﻿// <copyright file="LeadboardItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    /// <summary>
    /// Items to display on leadboard.
    /// </summary>
    public class LeadboardItem
    {
        /// <summary>
        /// Gets or sets name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets score.
        /// </summary>
        public int Score { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return this.Name + ", " + this.Score + " point";
        }
    }
}
