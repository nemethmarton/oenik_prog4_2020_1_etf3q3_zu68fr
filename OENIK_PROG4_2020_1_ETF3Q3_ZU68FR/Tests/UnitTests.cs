﻿// <copyright file="UnitTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Tests
{
    using System;
    using System.Collections.Generic;
    using Logic;
    using Model;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class for unit tests.
    /// </summary>
    [TestFixture]
    internal class UnitTests
    {
        /// <summary>
        /// Test players move.
        /// </summary>
        [Test]
        public void MovePlayerTest()
        {
            Mock<IGameModel> mockedModel = new Mock<IGameModel>(MockBehavior.Loose);
            GameLogic gl = new GameLogic(mockedModel.Object, 0);

            List<Enemy> testEnemies = new List<Enemy>();
            List<Ladder> testLadders = new List<Ladder>();
            List<Item> testItems = new List<Item>();
            List<Wall> testWalls = new List<Wall>();
            Player testPlayer = new Player(100, 100, 40, 40, 5, 3, 0);

            mockedModel.SetupGet(ex => ex.Player).Returns(() => testPlayer);
            mockedModel.SetupGet(ex => ex.Enemies).Returns(() => testEnemies);
            mockedModel.SetupGet(ex => ex.Ladders).Returns(() => testLadders);
            mockedModel.SetupGet(ex => ex.Items).Returns(() => testItems);
            mockedModel.SetupGet(ex => ex.Walls).Returns(() => testWalls);
            mockedModel.SetupGet(ex => ex.GameWidth).Returns(() => 600);
            mockedModel.SetupGet(ex => ex.GameHeight).Returns(() => 600);

            gl.MovePlayer(Direction.Right);

            Assert.That(mockedModel.Object.Player.Area.X == 105);
        }

        /// <summary>
        /// Test players gravity.
        /// </summary>
        [Test]
        public void PlayerGravityTest()
        {
            Mock<IGameModel> mockedModel = new Mock<IGameModel>(MockBehavior.Loose);
            GameLogic gl = new GameLogic(mockedModel.Object, 0);
            List<Enemy> testEnemies = new List<Enemy>();
            List<Ladder> testLadders = new List<Ladder>();
            List<Item> testItems = new List<Item>();
            List<Wall> testWalls = new List<Wall>();
            Player testPlayer = new Player(100, 100, 40, 40, 5, 3, 0);
            testWalls.Add(new Wall(0, 180, 200, 30));

            mockedModel.SetupGet(ex => ex.Player).Returns(() => testPlayer);
            mockedModel.SetupGet(ex => ex.Enemies).Returns(() => testEnemies);
            mockedModel.SetupGet(ex => ex.Ladders).Returns(() => testLadders);
            mockedModel.SetupGet(ex => ex.Items).Returns(() => testItems);
            mockedModel.SetupGet(ex => ex.Walls).Returns(() => testWalls);
            mockedModel.SetupGet(ex => ex.GameWidth).Returns(() => 600);
            mockedModel.SetupGet(ex => ex.GameHeight).Returns(() => 600);

            for (int i = 0; i < 100; i++)
            {
                gl.PlayerGravity();
            }

            Assert.That(mockedModel.Object.Player.Area.Bottom == 180);
        }

        /// <summary>
        /// Test game logic pause.
        /// </summary>
        [Test]
        public void PauseTest()
        {
            Mock<IGameModel> mockedModel = new Mock<IGameModel>(MockBehavior.Loose);
            GameLogic gl = new GameLogic(mockedModel.Object, 0);
            List<Enemy> testEnemies = new List<Enemy>();
            List<Ladder> testLadders = new List<Ladder>();
            List<Item> testItems = new List<Item>();
            List<Wall> testWalls = new List<Wall>();

            Player testPlayer = new Player(100, 100, 40, 40, 5, 3, 0);
            testWalls.Add(new Wall(0, 180, 200, 30));
            testEnemies.Add(new Enemy(200, 200, 30, 30, 5, 1, true));
            testEnemies.Add(new Enemy(300, 300, 30, 30, 5, 1, false));

            mockedModel.SetupGet(ex => ex.Player).Returns(() => testPlayer);
            mockedModel.SetupGet(ex => ex.Enemies).Returns(() => testEnemies);
            mockedModel.SetupGet(ex => ex.Ladders).Returns(() => testLadders);
            mockedModel.SetupGet(ex => ex.Walls).Returns(() => testWalls);
            mockedModel.SetupGet(ex => ex.Items).Returns(() => testItems);
            mockedModel.SetupGet(ex => ex.GameWidth).Returns(() => 600);
            mockedModel.SetupGet(ex => ex.GameHeight).Returns(() => 600);

            gl.Pause();
            for (int i = 0; i < 100; i++)
            {
                gl.PlayerGravity();
            }

            Random r = new Random();
            for (int i = 0; i < 10; i++)
            {
                gl.MovePlayer((Direction)r.Next(0, 4));
            }

            for (int i = 0; i < 100; i++)
            {
                gl.MoveEnemy();
            }

            gl.Pause();
            gl.PlayerGravity();
            gl.MovePlayer((Direction)r.Next(0, 4));
            gl.MoveEnemy();

            Assert.That(mockedModel.Object.Player.Area.X == 100);
            Assert.That(mockedModel.Object.Player.Area.Y == 105);
            Assert.That(mockedModel.Object.Enemies[0].Area.X == 205);
            Assert.That(mockedModel.Object.Enemies[0].Area.Y == 200);
            Assert.That(mockedModel.Object.Enemies[1].Area.X == 300);
            Assert.That(mockedModel.Object.Enemies[1].Area.Y == 300);
        }

        /// <summary>
        /// Test enemies freezing.
        /// </summary>
        [Test]
        public void FreezeTimeTest()
        {
            Mock<IGameModel> mockedModel = new Mock<IGameModel>(MockBehavior.Loose);
            GameLogic gl = new GameLogic(mockedModel.Object, 0);
            List<Enemy> testEnemies = new List<Enemy>();
            List<Ladder> testLadders = new List<Ladder>();
            List<Item> testItems = new List<Item>();
            List<Wall> testWalls = new List<Wall>();

            Player testPlayer = new Player(100, 100, 40, 40, 5, 3, 0);
            testWalls.Add(new Wall(0, 180, 200, 30));
            testEnemies.Add(new Enemy(200, 200, 30, 30, 5, 1, true));
            testEnemies.Add(new Enemy(300, 300, 30, 30, 5, 1, false));
            testItems.Add(new Item(100, 100, 30, 30, 2, true));

            mockedModel.SetupGet(ex => ex.Player).Returns(() => testPlayer);
            mockedModel.SetupGet(ex => ex.Enemies).Returns(() => testEnemies);
            mockedModel.SetupGet(ex => ex.Ladders).Returns(() => testLadders);
            mockedModel.SetupGet(ex => ex.Items).Returns(() => testItems);
            mockedModel.SetupGet(ex => ex.Walls).Returns(() => testWalls);
            mockedModel.SetupGet(ex => ex.GameWidth).Returns(() => 600);
            mockedModel.SetupGet(ex => ex.GameHeight).Returns(() => 600);

            gl.PlayerGravity();

            for (int i = 0; i < 100; i++)
            {
                gl.PlayerGravity();
                gl.MoveEnemy();
            }

            for (int i = 0; i < 1000; i++)
            {
                gl.DecreaseFreezeTime();
            }

            gl.MoveEnemy();

            Assert.That(mockedModel.Object.Player.Area.X == 100);
            Assert.That(mockedModel.Object.Player.Area.Bottom == 180);
            Assert.That(mockedModel.Object.Enemies[0].Area.X == 205);
            Assert.That(mockedModel.Object.Enemies[0].Area.Y == 200);
            Assert.That(mockedModel.Object.Enemies[1].Area.X == 300);
            Assert.That(mockedModel.Object.Enemies[1].Area.Y == 300);
        }

        /// <summary>
        /// Test stage start.
        /// </summary>
        [Test]
        public void StageStartTest()
        {
            Mock<IGameModel> mockedModel = new Mock<IGameModel>(MockBehavior.Loose);
            GameLogic gl = new GameLogic(mockedModel.Object, 0);

            string[] file = new string[3]
            {
                "Player;10;10;10;10;5;3;0",
                "Player;20;20;20;20;5;3;0",
                "Player;30;30;30;30;5;3;0",
            };
            string number = "2";

            List<Enemy> testEnemies = new List<Enemy>();
            Player testPlayer = new Player(100, 100, 40, 40, 5, 3, 0);

            mockedModel.SetupGet(ex => ex.Player).Returns(() => testPlayer);
            mockedModel.SetupGet(ex => ex.Enemies).Returns(() => testEnemies);
            mockedModel.SetupGet(ex => ex.GameWidth).Returns(() => 600);
            mockedModel.SetupGet(ex => ex.GameHeight).Returns(() => 600);
            mockedModel.Setup(model => model.InitializeStage(It.IsAny<string>())).Callback(
                (string stage) =>
                {
                    string stageNum = stage.Split('e')[1];
                    stageNum = stageNum.Split('.')[0];
                    string[] helper = file[int.Parse(stageNum) - 1].Split(';');
                    testPlayer = new Player(
                        int.Parse(helper[1]), int.Parse(helper[2]), int.Parse(helper[3]), int.Parse(helper[4]), int.Parse(helper[5]), int.Parse(helper[6]), int.Parse(helper[7]));
                    });

            gl.StageStart(number);
            Assert.That(testPlayer.Area.X == 10 * int.Parse(number));
            Assert.That(testPlayer.Area.Y == 10 * int.Parse(number));
            Assert.That(testPlayer.Area.Width == 10 * int.Parse(number));
            Assert.That(testPlayer.Area.Height == 10 * int.Parse(number));
        }
    }
}
