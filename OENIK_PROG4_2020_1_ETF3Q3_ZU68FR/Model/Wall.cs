﻿// <copyright file="Wall.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    /// <summary>
    /// Class for walls.
    /// </summary>
    public class Wall : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Wall"/> class.
        /// </summary>
        /// <param name="x">x position.</param>
        /// <param name="y">y position.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        public Wall(int x, int y, int w, int h)
            : base(x, y, w, h)
        {
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Wall;{this.Area.X};{this.Area.Y};{this.Area.Width};{this.Area.Height}";
        }
    }
}
