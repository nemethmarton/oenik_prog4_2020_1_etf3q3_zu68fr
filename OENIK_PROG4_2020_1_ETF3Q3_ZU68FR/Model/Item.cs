﻿// <copyright file="Item.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    /// <summary>
    /// Class for item.
    /// </summary>
    public class Item : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Item"/> class.
        /// </summary>
        /// <param name="x">x position.</param>
        /// <param name="y">y position.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        /// <param name="id">item id.</param>
        /// <param name="active">active.</param>
        public Item(int x, int y, int w, int h, int id, bool active)
            : base(x, y, w, h)
        {
            this.Active = active;
            this.Type = id;
        }

        /// <summary>
        /// Gets or sets type of item.
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether active of item.
        /// </summary>
        public bool Active { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Item;{this.Area.X};{this.Area.Y};{this.Area.Width};{this.Area.Height};{this.Type};{this.Active}";
        }
    }
}
