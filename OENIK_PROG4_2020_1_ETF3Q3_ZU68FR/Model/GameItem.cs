﻿// <copyright file="GameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System.Windows;

    /// <summary>
    /// Unifying class for game items.
    /// </summary>
    public abstract class GameItem
    {
        /// <summary>
        /// Area of game item.
        /// </summary>
        private protected Rect area;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameItem"/> class.
        /// Sets the area of game item.
        /// </summary>
        /// <param name="x">x position.</param>
        /// <param name="y">y position.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        public GameItem(int x, int y, int w, int h)
        {
            this.area = new Rect(x, y, w, h);
        }

        /// <summary>
        /// Gets the area of game item.
        /// </summary>
        public Rect Area
        {
            get { return this.area; }
        }
    }
}
