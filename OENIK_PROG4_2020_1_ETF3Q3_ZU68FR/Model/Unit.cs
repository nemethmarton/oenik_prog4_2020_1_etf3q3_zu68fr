﻿// <copyright file="Unit.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    /// <summary>
    /// Class for moving game items.
    /// </summary>
    public abstract class Unit : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Unit"/> class.
        /// </summary>
        /// <param name="x">x position.</param>
        /// <param name="y">y position.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        /// <param name="speed">speed.</param>
        /// <param name="active">active.</param>
        public Unit(int x, int y, int w, int h, int speed, bool active)
        : base(x, y, w, h)
        {
            this.Active = active;
            this.Speed = speed;
            this.Dx = speed;
        }

        /// <summary>
        /// Gets or sets a value indicating whether item active.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets speed.
        /// </summary>
        public int Speed { get; set; }

        /// <summary>
        /// Gets or sets direction x coordinate.
        /// </summary>
        public int Dx { get; set; }

        /// <summary>
        /// Gets or sets direction y coordinate.
        /// </summary>
        public int Dy { get; set; }

        /// <summary>
        /// Move the item by x position.
        /// </summary>
        /// <param name="diff">Added to x value.</param>
        public void MoveX(double diff)
        {
            this.area.X += diff;
        }

        /// <summary>
        /// Move the item by y position.
        /// </summary>
        /// <param name="diff">Added to y value.</param>
        public void MoveY(double diff)
        {
            this.area.Y += diff;
        }

        /// <summary>
        /// Sets x and y position.
        /// </summary>
        /// <param name="x">Set x value.</param>
        /// <param name="y">Set y value.</param>
        public void SetXY(double x, double y)
        {
            this.area.X = x;
            this.area.Y = y;
        }
    }
}
