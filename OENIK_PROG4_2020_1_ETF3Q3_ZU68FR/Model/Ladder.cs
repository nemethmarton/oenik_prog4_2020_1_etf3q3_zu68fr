﻿// <copyright file="Ladder.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    /// <summary>
    /// Class for ladder.
    /// </summary>
    public class Ladder : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Ladder"/> class.
        /// </summary>
        /// <param name="x">x position.</param>
        /// <param name="y">y position.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        public Ladder(int x, int y, int w, int h)
            : base(x, y, w, h)
        {
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Ladder;{this.Area.X};{this.Area.Y};{this.Area.Width};{this.Area.Height}";
        }
    }
}
