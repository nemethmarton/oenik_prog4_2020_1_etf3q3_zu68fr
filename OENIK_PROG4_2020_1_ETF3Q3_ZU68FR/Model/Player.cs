﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    /// <summary>
    /// Class for player.
    /// </summary>
    public class Player : Unit
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="x">x position.</param>
        /// <param name="y">y position.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        /// <param name="speed">speed.</param>
        /// <param name="health">health.</param>
        /// <param name="itemid">held item id.</param>
        public Player(int x, int y, int w, int h, int speed, int health, int itemid)
            : base(x, y, w, h, speed, true)
        {
            this.Health = health;
            this.HeldItemId = itemid;
            this.Dx = 0;
        }

        /// <summary>
        /// Gets or sets id of item hold by player.
        /// </summary>
        public int HeldItemId { get; set; }

        /// <summary>
        /// Gets or sets health of player.
        /// </summary>
        public int Health { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Player;{this.Area.X};{this.Area.Y};{this.Area.Width};{this.Area.Height};{this.Speed};{this.Health};{this.HeldItemId}";
        }
    }
}
