﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface for GameModel.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets score reached by player.
        /// </summary>
        int Score { get; set; }

        /// <summary>
        /// Gets player object.
        /// </summary>
        Player Player { get;  }

        /// <summary>
        /// Gets list of enemies.
        /// </summary>
        List<Enemy> Enemies { get;  }

        /// <summary>
        /// Gets list of items.
        /// </summary>
        List<Item> Items { get;  }

        /// <summary>
        /// Gets list of walls.
        /// </summary>
        List<Wall> Walls { get;  }

        /// <summary>
        /// Gets list of ladders.
        /// </summary>
        List<Ladder> Ladders { get; }

        /// <summary>
        /// Gets width of game area.
        /// </summary>
        double GameWidth { get;  }

        /// <summary>
        /// Gets height of game area.
        /// </summary>
        double GameHeight { get;  }

        /// <summary>
        /// Gets or sets size of 1 tile.
        /// </summary>
        double TileSize { get; set; }

        /// <summary>
        /// Load one stage.
        /// </summary>
        /// <param name="name">Stage name.</param>
        void InitializeStage(string name);

        /// <summary>
        /// Save the game state.
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <param name="freezeTimeLeft">Freeze time of enemies.</param>
        /// <param name="gravityPulls">Gravity exists.</param>
        /// <param name="enemiesFrozen">Enemies frozen.</param>
        /// <param name="stageNumber">Stage number.</param>
        void SaveGame(string fileName, int freezeTimeLeft, bool gravityPulls, bool enemiesFrozen, int stageNumber);

        /// <summary>
        /// Setup parameters when game loading.
        /// </summary>
        /// <param name="freezeTimeLeft">Freeze time of enemies.</param>
        /// <param name="gravityPulls">Gravity exists.</param>
        /// <param name="enemiesFrozen">Enemies frozen.</param>
        /// <param name="stageNumber">Stage number.</param>
        void EtcSetup(ref int freezeTimeLeft, ref bool gravityPulls, ref bool enemiesFrozen, ref int stageNumber);
    }
}
