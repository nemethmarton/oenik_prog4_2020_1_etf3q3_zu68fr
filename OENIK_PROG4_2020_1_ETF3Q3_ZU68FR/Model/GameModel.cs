﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    using System;
    using System.Collections.Generic;
    using Repository;

    /// <summary>
    /// Class for Game Model.
    /// </summary>
    public class GameModel : IGameModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        public GameModel(double w, double h)
        {
            this.Score = 0;
            this.TileSize = 40;
            this.GameWidth = w;
            this.GameHeight = h;
        }

        /// <inheritdoc/>
        public int Score { get; set; }

        /// <inheritdoc/>
        public Player Player { get; private set; }

        /// <inheritdoc/>
        public List<Enemy> Enemies { get; private set; }

        /// <inheritdoc/>
        public List<Item> Items { get; private set; }

        /// <inheritdoc/>
        public List<Wall> Walls { get; private set; }

        /// <inheritdoc/>
        public List<Ladder> Ladders { get; private set; }

        /// <inheritdoc/>
        public double GameWidth { get; private set; }

        /// <inheritdoc/>
        public double GameHeight { get; private set; }

        /// <inheritdoc/>
        public double TileSize { get; set; }

        /// <inheritdoc/>
        public void InitializeStage(string stageName)
        {
            XML.LoadGame(stageName);
            this.Player = new Player(220, 60, 40, 40, 5, 3, 0);
            this.Enemies = new List<Enemy>();
            this.Walls = new List<Wall>();
            this.Items = new List<Item>();
            this.Ladders = new List<Ladder>();

            foreach (var gameItem in XML.GameItems)
                {
                    string[] helper = gameItem.Split(';');
                    switch (helper[0])
                    {
                        case "Player":
                            this.Player = new Player(this.Int(helper[1]), this.Int(helper[2]), this.Int(helper[3]), this.Int(helper[4]), this.Int(helper[5]), this.Int(helper[6]), this.Int(helper[7]));
                            break;
                        case "Wall":
                            this.Walls.Add(new Wall(this.Int(helper[1]), this.Int(helper[2]), this.Int(helper[3]), this.Int(helper[4])));
                            break;
                        case "Ladder":
                            this.Ladders.Add(new Ladder(this.Int(helper[1]), this.Int(helper[2]), this.Int(helper[3]), this.Int(helper[4])));
                            break;
                        case "Item":
                            this.Items.Add(new Item(this.Int(helper[1]), this.Int(helper[2]), this.Int(helper[3]), this.Int(helper[4]), this.Int(helper[5]), this.Bool(helper[6])));
                            break;
                        case "Enemy":
                            this.Enemies.Add(new Enemy(this.Int(helper[1]), this.Int(helper[2]), this.Int(helper[3]), this.Int(helper[4]), this.Int(helper[5]), this.Int(helper[6]), this.Bool(helper[7])));
                            break;
                        default:
                            break;
                    }
                }
        }

        /// <inheritdoc/>
        public void SaveGame(string fileName, int freezeTimeLeft, bool gravityPulls, bool enemiesFrozen, int stageNumber)
        {
            string etc = $"{freezeTimeLeft};{gravityPulls};{enemiesFrozen};{stageNumber};{this.Score}";
            List<string> gameItems = new List<string>
            {
                this.Player.ToString(),
            };
            foreach (var item in this.Enemies)
            {
                gameItems.Add(item.ToString());
            }

            foreach (var item in this.Items)
            {
                gameItems.Add(item.ToString());
            }

            foreach (var item in this.Walls)
            {
                gameItems.Add(item.ToString());
            }

            foreach (var item in this.Ladders)
            {
                gameItems.Add(item.ToString());
            }

            Repository.XML.SaveGame(fileName, etc, gameItems);
        }

        /// <inheritdoc/>
        public void EtcSetup(ref int freezeTimeLeft, ref bool gravityPulls, ref bool enemiesFrozen, ref int stageNumber)
        {
            string[] etc = XML.Etc.Split(';');
            freezeTimeLeft = this.Int(etc[0]);
            gravityPulls = this.Bool(etc[1]);
            enemiesFrozen = this.Bool(etc[2]);
            stageNumber = this.Int(etc[3]);
            this.Score = this.Int(etc[4]);
        }

        private int Int(string data)
        {
            return Convert.ToInt32(data);
        }

        private bool Bool(string data)
        {
            return Convert.ToBoolean(data);
        }
    }
}
