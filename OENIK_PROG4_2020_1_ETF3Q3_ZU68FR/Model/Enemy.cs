﻿// <copyright file="Enemy.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Model
{
    /// <summary>
    /// Class for enemies.
    /// </summary>
    public class Enemy : Unit
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Enemy"/> class.
        /// </summary>
        /// <param name="x">x position.</param>
        /// <param name="y">y position.</param>
        /// <param name="w">width.</param>
        /// <param name="h">height.</param>
        /// <param name="speed">speed.</param>
        /// <param name="type">type.</param>
        /// <param name="active">active.</param>
        public Enemy(int x, int y, int w, int h, int speed, int type, bool active)
            : base(x, y, w, h, speed, active)
        {
            this.Type = type;
        }

        /// <summary>
        /// Gets or sets type of enemy.
        /// </summary>
        public int Type { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"Enemy;{this.Area.X};{this.Area.Y};{this.Area.Width};{this.Area.Height};{this.Speed};{this.Type};{this.Active}";
        }
    }
}
