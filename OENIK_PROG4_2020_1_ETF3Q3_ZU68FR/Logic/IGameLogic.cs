﻿// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;

    /// <summary>
    /// Interface for Game Logic.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Event for renderer to refresh the screen.
        /// </summary>
        event EventHandler RefreshScreen;

        /// <summary>
        /// Move the player with the specified direction.
        /// </summary>
        /// <param name="direction">Direction of move.</param>
        void MovePlayer(Direction direction);

        /// <summary>
        /// Enemy moving logic.
        /// </summary>
        void MoveEnemy();

        /// <summary>
        /// Countdown to defreeze enemies.
        /// </summary>
        void DecreaseFreezeTime();

        /// <summary>
        /// Load a stage.
        /// </summary>
        /// <param name="stageNum">Level of stage.</param>
        void StageStart(string stageNum);

        /// <summary>
        /// Pause the logic.
        /// </summary>
        void Pause();

        /// <summary>
        /// Implements gravity, player can fall off the walls.
        /// </summary>
        void PlayerGravity();

        /// <summary>
        /// Save game to a specified xml file.
        /// </summary>
        /// <param name="v">Name of savegame file.</param>
        void SaveGame(string v);

        /// <summary>
        /// Get the score for highscore saving.
        /// </summary>
        /// <returns>Current game score.</returns>
        int GetScore();

        /// <summary>
        /// True when game is over.
        /// </summary>
        /// <returns>State of game.</returns>
        bool GetGameOver();
    }
}
