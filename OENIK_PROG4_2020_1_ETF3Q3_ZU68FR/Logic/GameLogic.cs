﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Model;

    /// <summary>
    /// Direction of move. Values form 0 to 3.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// 0.
        /// </summary>
        Left,

        /// <summary>
        /// 1.
        /// </summary>
        Right,

        /// <summary>
        /// 2.
        /// </summary>
        Up,

        /// <summary>
        /// 3.
        /// </summary>
        Down,
    }

    /// <summary>
    /// Business logic, game rules.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        private static int gameID = 0;
        private readonly int gravityStrength = 5;
        private readonly int numberOfStages = 3;
        private readonly IGameModel model;
        private readonly int logicID;
        private int freezeTimeLeft = 0;
        private bool gravityPulls = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="model">Instance of GameModel.</param>
        /// <param name="loadType">New game (0), load saved game (1) or resume paused game (2).</param>
        public GameLogic(IGameModel model, int loadType)
        {
            gameID++;
            this.logicID = gameID;
            this.model = model;
            this.CurrentStageNum = 1;
            if (loadType == 0)
            {
                this.StageStart(this.CurrentStageNum.ToString());
            }
            else if (loadType == 1)
            {
                model.InitializeStage("savedGame.xml");
                bool enFr = false;
                int stageNum = 0;
                model.EtcSetup(ref this.freezeTimeLeft, ref this.gravityPulls, ref enFr, ref stageNum);
                this.CurrentStageNum = stageNum;
                this.EnemiesFrozen = enFr;
                this.GameRunning = true;
            }
            else if (loadType == 2)
            {
                model.InitializeStage("pauseState.xml");
                bool enFr = false;
                int stageNum = 0;
                model.EtcSetup(ref this.freezeTimeLeft, ref this.gravityPulls, ref enFr, ref stageNum);
                this.CurrentStageNum = stageNum;
                this.EnemiesFrozen = enFr;
                this.GameRunning = true;
            }
        }

        /// <summary>
        /// Event to notify GameRenderer about changes.
        /// </summary>
        public event EventHandler RefreshScreen;

        /// <summary>
        /// Gets or sets a value indicating whether true when game ended.
        /// </summary>
        public bool GameIsOver { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether true when enemies can't move.
        /// </summary>
        private bool EnemiesFrozen { get; set; }

        /// <summary>
        /// Gets or sets the number of stage/level.
        /// </summary>
        private int CurrentStageNum { get; set; }

        private bool GameRunning { get; set; }

        /// <summary>
        /// Countdown to defreeze enemies.
        /// </summary>
        public void DecreaseFreezeTime()
        {
            if (this.EnemiesFrozen && this.GameRunning)
            {
                this.freezeTimeLeft--;

                if (this.freezeTimeLeft == 0)
                {
                    this.EnemiesFrozen = false;
                }
            }
        }

        /// <summary>
        /// Load a stage.
        /// </summary>
        /// <param name="stageNum">Level of stage.</param>
        public void StageStart(string stageNum)
        {
            string stg = "stage" + stageNum + ".xml";
            this.model.InitializeStage(stg);
            this.EnemiesFrozen = false;
            this.freezeTimeLeft = 0;
            this.gravityPulls = false;
            this.GameRunning = true;
        }

        /// <summary>
        /// Pause the logic.
        /// </summary>
        public void Pause()
        {
            if (this.logicID == gameID)
            {
                this.GameRunning = !this.GameRunning;
            }
        }

        /// <summary>
        /// Implements gravity, player can fall off the walls.
        /// </summary>
        public void PlayerGravity()
        {
            if (this.GameRunning)
            {
                bool gravityWillPull = true;
                List<GameItem> collidesWith = this.CollidesWith(this.model.Player);
                foreach (var gameItem in collidesWith)
                {
                    if (gameItem is Item)
                    {
                        this.GetItem(gameItem as Item);
                    }
                    else if (gameItem is Enemy)
                    {
                        this.PlayerEnemyHit(gameItem as Enemy);
                    }
                    else if (gameItem is Ladder)
                    {
                        gravityWillPull = false;
                    }
                    else if (gameItem is Wall && this.model.Player.Area.Bottom == gameItem.Area.Top && true
                            && (this.model.Player.Area.Left < gameItem.Area.Right && this.model.Player.Area.Right > gameItem.Area.Left))
                    {
                        gravityWillPull = false;
                    }
                }

                if (gravityWillPull)
                {
                    this.gravityPulls = true;
                }
                else
                {
                    this.gravityPulls = false;
                }

                if (this.gravityPulls)
                {
                    this.model.Player.MoveY(this.gravityStrength);
                    if (this.model.Player.Area.Y > this.model.GameHeight)
                    {
                        this.GameOver();
                    }
                }

                this.RefreshScreen?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Move the player with the specified direction.
        /// </summary>
        /// <param name="d">Direction of move.</param>
        public void MovePlayer(Direction d)
        {
            if (this.GameRunning)
            {
                if (d == Direction.Left)
                {
                    this.model.Player.Dx = -this.model.Player.Speed;
                }
                else if (d == Direction.Right)
                {
                    this.model.Player.Dx = this.model.Player.Speed;
                }
                else if (d == Direction.Up)
                {
                    this.model.Player.Dy = -this.model.Player.Speed;
                }
                else if (d == Direction.Down)
                {
                    this.model.Player.Dy = this.model.Player.Speed;
                }

                bool[] forbiddenDir = new bool[4] { false, false, true, true };

                if (this.model.Player.Area.Left == 0)
                {
                    forbiddenDir[0] = true;
                }

                if (this.model.Player.Area.Right == this.model.GameWidth)
                {
                    forbiddenDir[1] = true;
                }

                if (this.model.Player.Area.Top == 0)
                {
                    forbiddenDir[2] = true;
                }

                if (this.model.Player.Area.Bottom == this.model.GameHeight)
                {
                    forbiddenDir[3] = true;
                }

                List<GameItem> collidesWith = this.CollidesWith(this.model.Player);
                foreach (var gameItem in collidesWith)
                {
                    if (gameItem is Ladder)
                    {
                        forbiddenDir[2] = false; // up
                        forbiddenDir[3] = false; // down
                    }

                    if (gameItem is Wall)
                    {
                        if (this.model.Player.Area.Left == gameItem.Area.Right
                            && (this.model.Player.Area.Bottom > gameItem.Area.Top && this.model.Player.Area.Top < gameItem.Area.Bottom))
                        {
                            forbiddenDir[0] = true; // left
                        }

                        if (this.model.Player.Area.Right == gameItem.Area.Left
                            && (this.model.Player.Area.Bottom > gameItem.Area.Top && this.model.Player.Area.Top < gameItem.Area.Bottom))
                        {
                            forbiddenDir[1] = true; // right
                        }

                        if (this.model.Player.Area.Top == gameItem.Area.Bottom && true
                           && (this.model.Player.Area.Left < gameItem.Area.Right && this.model.Player.Area.Right > gameItem.Area.Left))
                        {
                            forbiddenDir[2] = true; // up
                        }

                        if (this.model.Player.Area.Bottom == gameItem.Area.Top && true
                            && (this.model.Player.Area.Left < gameItem.Area.Right && this.model.Player.Area.Right > gameItem.Area.Left))
                        {
                            forbiddenDir[3] = true; // down
                        }
                    }

                    if (gameItem is Item)
                    {
                        this.GetItem(gameItem as Item);
                    }

                    if (gameItem is Enemy)
                    {
                        this.PlayerEnemyHit(gameItem as Enemy);
                    }
                }

                if (!forbiddenDir[Convert.ToInt32(d)] && !this.gravityPulls)
                {
                    this.model.Player.MoveX(this.model.Player.Dx);
                    this.model.Player.MoveY(this.model.Player.Dy);
                }

                this.model.Player.Dx = 0;
                this.model.Player.Dy = 0;

                this.RefreshScreen?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Enemy moving logic.
        /// </summary>
        public void MoveEnemy()
        {
            if (this.GameRunning && !this.EnemiesFrozen)
            {
                foreach (var enemy in this.model.Enemies)
                {
                    if (enemy.Active)
                    {
                        bool reverseDirection = false;
                        List<GameItem> collidesWith = this.CollidesWith(enemy);
                        foreach (var item in collidesWith)
                        {
                            if (item is Wall
                                && (item.Area.Right == enemy.Area.Right || item.Area.Left == enemy.Area.Left))
                            {
                                reverseDirection = true;
                            }

                            if (item is Player)
                            {
                                this.PlayerEnemyHit(enemy);
                            }
                        }

                        if (enemy.Type == 2)
                        {
                            enemy.Dx = (enemy.Area.Left + (enemy.Area.Right / 2)) >= (this.model.Player.Area.Left + (this.model.Player.Area.Right / 2)) ?
                                 enemy.Speed * -1 : enemy.Speed;
                            if (this.model.Player.HeldItemId != 0)
                            {
                                enemy.Dx *= -1;
                            }
                        }

                        if (reverseDirection)
                        {
                            enemy.Dx *= -1;
                        }

                        enemy.MoveX(enemy.Dx);
                    }
                }

                this.RefreshScreen?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Save game to a specified xml file.
        /// </summary>
        /// <param name="fileName">Name of savegame file.</param>
        public void SaveGame(string fileName)
        {
            this.model.SaveGame(fileName, this.freezeTimeLeft, this.gravityPulls, this.EnemiesFrozen, this.CurrentStageNum);
        }

        /// <summary>
        /// Get the score for highscore saving.
        /// </summary>
        /// <returns>Current game score.</returns>
        public int GetScore()
        {
            return this.model.Score;
        }

        /// <summary>
        /// True when game is over.
        /// </summary>
        /// <returns>State of game.</returns>
        public bool GetGameOver()
        {
            if (gameID == this.logicID)
            {
                return this.GameIsOver;
            }

            return false;
        }

        private void CheckIfWon()
        {
            if (this.model.Enemies.Where(x => x.Active == true).FirstOrDefault() == null)
            {
                this.GameRunning = false;
                if (this.CurrentStageNum != this.numberOfStages)
                {
                    this.CurrentStageNum++;
                    this.StageStart(this.CurrentStageNum.ToString());
                }
                else
                {
                    this.GameOver();
                }
            }
        }

        private void PlayerEnemyHit(Enemy enemy)
        {
            if (this.model.Player.HeldItemId == 1)
            {
                this.model.Player.HeldItemId = 0;
                enemy.Active = false;
                this.model.Score++;
                this.CheckIfWon();
            }
            else
            {
                this.GameOver();
            }
        }

        private void GetItem(Item gameItem)
        {
            if (gameItem.Type == 1 && this.model.Player.HeldItemId == 0)
            {
                this.model.Player.HeldItemId = (gameItem as Item).Type;
                gameItem.Active = false;
            }
            else if (gameItem.Type == 2)
            {
                this.EnemiesFrozen = true;
                this.freezeTimeLeft = 200;
                gameItem.Active = false;
            }
        }

        private List<GameItem> CollidesWith(GameItem current)
        {
            List<GameItem> collidesWith = new List<GameItem>();
            foreach (var item in this.model.Enemies)
            {
                if (item.Active && current.Area.IntersectsWith(item.Area))
                {
                    collidesWith.Add(item);
                }
            }

            foreach (var item in this.model.Ladders)
            {
                if (current.Area.IntersectsWith(item.Area))
                {
                    collidesWith.Add(item);
                }
            }

            foreach (var item in this.model.Walls)
            {
                if (current.Area.IntersectsWith(item.Area))
                {
                    collidesWith.Add(item);
                }
            }

            foreach (var item in this.model.Items)
            {
                if (item.Active && current.Area.IntersectsWith(item.Area))
                {
                    collidesWith.Add(item);
                }
            }

            if (current.Area.IntersectsWith(this.model.Player.Area))
            {
                collidesWith.Add(this.model.Player);
            }

            return collidesWith;
        }

        private void GameOver()
        {
            this.GameIsOver = true;
            this.Pause();
        }
    }
}
