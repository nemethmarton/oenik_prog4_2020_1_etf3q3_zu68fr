﻿// <copyright file="GameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp
{
    using System;
    using System.Collections.Generic;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Model;

    /// <summary>
    /// Class for GameRenderer.
    /// </summary>
    public class GameRenderer
    {
        private readonly IGameModel model;
        private readonly Dictionary<string, Brush> myBrushes = new Dictionary<string, Brush>();
        private Drawing oldBackground;
        private Drawing oldWalls;
        private Drawing oldLadders;
        private Drawing oldPlayer;
        private Point oldPlayerPosition;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRenderer"/> class.
        /// </summary>
        /// <param name="model">GameModel.</param>
        public GameRenderer(IGameModel model)
        {
            this.model = model;
        }

        private Brush PlayerBrush
        {
            get { return this.GetBrush("WpfApp.img.player.png", false); }
        }

        private Brush WallBrush
        {
            get { return this.GetBrush("WpfApp.img.wall.png", true); }
        }

        private Brush LadderBrush
        {
            get { return this.GetBrush("WpfApp.img.ladder.png", true); }
        }

        private Brush EnemyBrush1
        {
            get { return this.GetBrush("WpfApp.img.enemy1.png", false); }
        }

        private Brush EnemyBrush2
        {
            get { return this.GetBrush("WpfApp.img.enemy2.png", false); }
        }

        private Brush ItemBrush1
        {
            get { return this.GetBrush("WpfApp.img.item1.png", false); }
        }

        private Brush ItemBrush2
        {
            get { return this.GetBrush("WpfApp.img.item2.png", false); }
        }

        private Brush BackgroundBrush
        {
            get { return this.GetBrush("WpfApp.img.background1.bmp", false); }
        }

        /// <summary>
        /// Reset Game view.
        /// </summary>
        public void Reset()
        {
            this.oldBackground = null;
            this.oldLadders = null;
            this.oldWalls = null;
            this.oldPlayer = null;
            this.oldPlayerPosition = new Point(-1, -1);
            this.myBrushes.Clear();
        }

        /// <summary>
        /// Combines elements to a drawing.
        /// </summary>
        /// <returns>DrawingGroup.</returns>
        [Obsolete]
        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();

            FormattedText formattedText = new FormattedText(
                this.model.Score.ToString(),
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Arial"),
                32,
                Brushes.Black);
            GeometryDrawing text = new GeometryDrawing(null, new Pen(Brushes.Red, 2), formattedText.BuildGeometry(new Point(5, 5)));

            dg.Children.Add(this.GetBackground());
            dg.Children.Add(this.GetLadders());
            dg.Children.Add(this.GetWalls());
            foreach (var item in this.model.Items)
            {
                if (item.Active)
                {
                    dg.Children.Add(this.GetItemTest(item));
                }
            }

            foreach (var item in this.model.Enemies)
            {
                if (item.Active)
                {
                    dg.Children.Add(this.GetEnemyTest(item));
                }
            }

            dg.Children.Add(this.GetPlayer());
            dg.Children.Add(text);
            return dg;
        }

        /// <summary>
        /// Create brush from image.
        /// </summary>
        /// <param name="fname">file name.</param>
        /// <param name="isTiled">tiled if true.</param>
        /// <returns>Brush.</returns>
        private Brush GetBrush(string fname, bool isTiled)
        {
            if (!this.myBrushes.ContainsKey(fname))
            {
                using (MemoryStream memory = new MemoryStream())
                {
                    System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(Assembly.GetExecutingAssembly().GetManifestResourceStream(fname));
                    bitmap.Save(memory, ImageFormat.Png);
                    memory.Position = 0;
                    BitmapImage bitmapImage = new BitmapImage();
                    bitmapImage.BeginInit();
                    bitmapImage.StreamSource = memory;
                    bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapImage.EndInit();
                    ImageBrush ib = new ImageBrush(bitmapImage);

                    if (isTiled)
                    {
                        ib.TileMode = TileMode.Tile;
                        ib.Viewport = new Rect(0, 0, this.model.TileSize, this.model.TileSize);
                        ib.ViewportUnits = BrushMappingMode.Absolute;
                    }

                    this.myBrushes[fname] = ib;
                }
            }

            return this.myBrushes[fname];
        }

        private Drawing GetBackground()
        {
            if (this.oldBackground == null)
            {
                Geometry backgroundGeometry = new RectangleGeometry(new Rect(0, 0, this.model.GameWidth, this.model.GameHeight));
                this.oldBackground = new GeometryDrawing(this.BackgroundBrush, null, backgroundGeometry);
            }

            return this.oldBackground;
        }

        private Drawing GetPlayer()
        {
            if (this.oldPlayer == null || (this.oldPlayerPosition.X != this.model.Player.Area.X || this.oldPlayerPosition.Y != this.model.Player.Area.Y))
            {
                Geometry g = new RectangleGeometry(new Rect(this.model.Player.Area.X, this.model.Player.Area.Y, this.model.Player.Area.Width, this.model.Player.Area.Height));
                this.oldPlayer = new GeometryDrawing(this.PlayerBrush, null, g);
                this.oldPlayerPosition.X = this.model.Player.Area.X;
                this.oldPlayerPosition.Y = this.model.Player.Area.Y;
            }

            return this.oldPlayer;
        }

        private Drawing GetItemTest(Item item)
        {
            Drawing tempItem = null;
            Geometry g = new RectangleGeometry(new Rect(item.Area.X, item.Area.Y, item.Area.Width, item.Area.Height));
            if (item.Type == 1)
            {
                tempItem = new GeometryDrawing(this.ItemBrush1, null, g);
            }
            else if (item.Type == 2)
            {
                tempItem = new GeometryDrawing(this.ItemBrush2, null, g);
            }

            return tempItem;
        }

        private Drawing GetEnemyTest(Enemy item)
        {
            Drawing tempEnemy = null;
            Geometry g = new RectangleGeometry(new Rect(item.Area.X, item.Area.Y, item.Area.Width, item.Area.Height));
            if (item.Type == 1)
            {
                tempEnemy = new GeometryDrawing(this.EnemyBrush1, null, g);
            }
            else if (item.Type == 2)
            {
                tempEnemy = new GeometryDrawing(this.EnemyBrush2, null, g);
            }

            return tempEnemy;
        }

        private Drawing GetLadders()
        {
            GeometryGroup g = new GeometryGroup();
            foreach (var ladder in this.model.Ladders)
            {
                Geometry box = new RectangleGeometry(new Rect(ladder.Area.X, ladder.Area.Y, ladder.Area.Width, ladder.Area.Height));
                g.Children.Add(box);
            }

            this.oldLadders = new GeometryDrawing(this.LadderBrush, null, g);
            return this.oldLadders;
        }

        private Drawing GetWalls()
        {
            GeometryGroup g = new GeometryGroup();
            foreach (var wall in this.model.Walls)
            {
                Geometry box = new RectangleGeometry(new Rect(wall.Area.X, wall.Area.Y, wall.Area.Width, wall.Area.Height));
                g.Children.Add(box);
            }

            this.oldWalls = new GeometryDrawing(this.WallBrush, null, g);
            return this.oldWalls;
        }
    }
}
