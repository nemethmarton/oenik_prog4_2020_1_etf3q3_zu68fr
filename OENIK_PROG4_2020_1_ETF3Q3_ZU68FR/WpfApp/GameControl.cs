﻿// <copyright file="GameControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using Logic;
    using Model;

    /// <summary>
    /// Class for GameControl.
    /// </summary>
    public class GameControl : FrameworkElement
    {
        private static bool gameOverHappened = false;
        private IGameLogic logic;
        private IGameModel model;
        private GameRenderer renderer;
        private Stopwatch stw;
        private DispatcherTimer tickTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// </summary>
        public GameControl()
        {
            gameOverHappened = false;
            this.Loaded += this.GameControl_Loaded;
        }

        /// <summary>
        /// Gets or sets load type for new game/load/resume.
        /// </summary>
        public int LoadType { get; set; }

        /// <summary>
        /// Overrides OnRenderer to draw game.
        /// </summary>
        /// <param name="drawingContext">renderer passed to it.</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                drawingContext.DrawDrawing(this.renderer.BuildDrawing());
            }
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.stw = new Stopwatch();
            this.model = new GameModel(this.ActualWidth, this.ActualHeight);
            this.logic = new GameLogic(this.model, this.LoadType);
            this.renderer = new GameRenderer(this.model);

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                win.KeyDown += this.Win_KeyDown;
            }

            this.InvalidateVisual();

            this.logic.RefreshScreen += (obj, args) => this.InvalidateVisual();
            this.tickTimer = new DispatcherTimer
            {
                Interval = TimeSpan.FromMilliseconds(25),
            };
            this.tickTimer.Tick += this.Timer_Tick;
            this.tickTimer.Start();
            this.stw.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            this.logic.DecreaseFreezeTime();
            this.logic.MoveEnemy();
            this.logic.PlayerGravity();

            if (this.logic.GetGameOver() && !gameOverHappened)
            {
                gameOverHappened = true;
                Navigator.End(this.model.Score);
            }
        }

        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.W:
                    this.logic.MovePlayer(Direction.Up);
                    break;
                case Key.S:
                    this.logic.MovePlayer(Direction.Down);
                    break;
                case Key.A:
                    this.logic.MovePlayer(Direction.Left);
                    break;
                case Key.D:
                    this.logic.MovePlayer(Direction.Right);
                    break;
                case Key.Escape:
                    this.logic.Pause();
                    this.logic.SaveGame("pauseState.xml");
                    Navigator.Pause(this.logic);
                    break;
            }

            this.InvalidateVisual();
        }
    }
}
