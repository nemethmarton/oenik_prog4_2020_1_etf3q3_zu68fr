﻿// <copyright file="Navigator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp
{
    using System.Windows;
    using WpfApp.Pages;

    /// <summary>
    /// Class for page navigation.
    /// </summary>
    public static class Navigator
    {
        private static readonly MenuPage menuPage = new MenuPage();
        private static readonly LeadboardPage leadboardPage = new LeadboardPage();
        private static PausePage pausePage;
        private static GamePage gamePage;
        private static EndgamePage endgamePage;

        /// <summary>
        /// Show menu.
        /// </summary>
        public static void Menu()
        {
            Application.Current.MainWindow.Content = menuPage;
        }

        /// <summary>
        /// Show leadboard.
        /// </summary>
        public static void Leadboard()
        {
            Application.Current.MainWindow.Content = leadboardPage;
        }

        /// <summary>
        /// Show pause menu.
        /// </summary>
        /// <param name="logic">GameLogic for save.</param>
        public static void Pause(Logic.IGameLogic logic)
        {
            pausePage = new PausePage(logic);
            Application.Current.MainWindow.Content = pausePage;
        }

        /// <summary>
        /// Show Game.
        /// </summary>
        /// <param name="betoltesTipus">Type of loading.</param>
        public static void Game(int betoltesTipus)
        {
            gamePage = new GamePage(betoltesTipus);
            Application.Current.MainWindow.Content = gamePage;
        }

        /// <summary>
        /// Show EndGame screen.
        /// </summary>
        /// <param name="score">Score to save.</param>
        public static void End(int score)
        {
            endgamePage = new EndgamePage(score);
            Application.Current.MainWindow.Content = endgamePage;
        }
    }
}
