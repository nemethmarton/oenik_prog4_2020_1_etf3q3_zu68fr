﻿// <copyright file="GamePage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp.Pages
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for GamePage.xaml.
    /// </summary>
    public partial class GamePage : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GamePage"/> class.
        /// </summary>
        /// <param name="betoltesTipus">Type of loading.</param>
        public GamePage(int betoltesTipus)
        {
            this.Resources.Add("betoltTipus", betoltesTipus);
            this.InitializeComponent();
        }
    }
}
