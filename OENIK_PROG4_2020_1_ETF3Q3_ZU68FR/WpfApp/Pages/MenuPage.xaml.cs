﻿// <copyright file="MenuPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for MenuPage.xaml.
    /// </summary>
    public partial class MenuPage : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuPage"/> class.
        /// </summary>
        public MenuPage()
        {
            this.InitializeComponent();
        }

        private void NewGameButtonClick(object sender, RoutedEventArgs e)
        {
            Navigator.Game(0);
        }

        private void LeadboardButtonClick(object sender, RoutedEventArgs e)
        {
            Navigator.Leadboard();
        }

        private void LoadGameButtonClick(object sender, RoutedEventArgs e)
        {
            Navigator.Game(1);
        }
    }
}
