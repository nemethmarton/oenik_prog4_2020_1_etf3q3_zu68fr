﻿// <copyright file="EndgamePage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp.Pages
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for EndgamePage.xaml.
    /// </summary>
    public partial class EndgamePage : Page
    {
        private readonly int score;

        /// <summary>
        /// Initializes a new instance of the <see cref="EndgamePage"/> class.
        /// </summary>
        /// <param name="score">score to save.</param>
        public EndgamePage(int score)
        {
            this.InitializeComponent();
            this.score = score;
        }

        private void SubmitScoreButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.nameInput.Text.Length > 0)
            {
                bool write = true;
                Repository.LeadboardItem itemToWrite = new Repository.LeadboardItem
                {
                    Name = this.nameInput.Text,
                    Score = this.score,
                };

                foreach (var item in Repository.XML.leadboard)
                {
                    if (item.Name == itemToWrite.Name)
                    {
                        if (item.Score >= itemToWrite.Score)
                        {
                            write = false;
                        }
                    }
                }

                if (write)
                {
                    Repository.XML.WriteLeadboard(itemToWrite);
                }

                MessageBox.Show("Thank you for playing");
            }

            Navigator.Menu();
        }
    }
}
