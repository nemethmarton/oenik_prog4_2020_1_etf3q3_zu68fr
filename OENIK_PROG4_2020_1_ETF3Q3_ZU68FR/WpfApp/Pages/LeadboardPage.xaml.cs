﻿// <copyright file="LeadboardPage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp.Pages
{
    using System.Windows;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for LeadboardPage.xaml.
    /// </summary>
    public partial class LeadboardPage : Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LeadboardPage"/> class.
        /// </summary>
        public LeadboardPage()
        {
            this.InitializeComponent();
            this.lbDataBinding.ItemsSource = Repository.XML.leadboard;
        }

        private void BackButtonClick(object sender, RoutedEventArgs e)
        {
            Navigator.Menu();
        }
    }
}
