﻿// <copyright file="PausePage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp.Pages
{
    using System.Windows;
    using System.Windows.Controls;
    using Logic;

    /// <summary>
    /// Interaction logic for PausePage.xaml.
    /// </summary>
    public partial class PausePage : Page
    {
        private readonly IGameLogic logic;

        /// <summary>
        /// Initializes a new instance of the <see cref="PausePage"/> class.
        /// </summary>
        /// <param name="logic">GameLogic.</param>
        public PausePage(IGameLogic logic)
        {
            this.logic = logic;
            this.InitializeComponent();
        }

        private void BackButtonClick(object sender, RoutedEventArgs e)
        {
            Navigator.Game(2);
        }

        private void SaveButtonClick(object sender, RoutedEventArgs e)
        {
            this.logic.SaveGame("savedGame.xml");
            Navigator.Menu();
        }

        private void ExitButtonClick(object sender, RoutedEventArgs e)
        {
            Navigator.Menu();
        }

        private void SaveScoreButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.nameInput.Text.Length > 0)
            {
                Repository.LeadboardItem itemToWrite = new Repository.LeadboardItem
                {
                    Name = this.nameInput.Text,
                    Score = this.logic.GetScore(),
                };

                foreach (var item in Repository.XML.leadboard)
                {
                    if (item.Name == itemToWrite.Name)
                    {
                        if (item.Score >= itemToWrite.Score)
                        {
                            MessageBox.Show("Not a highscore");
                            return;
                        }
                    }
                }

                Repository.XML.WriteLeadboard(itemToWrite);
                MessageBox.Show("Save succeeded");
            }
        }
    }
}
