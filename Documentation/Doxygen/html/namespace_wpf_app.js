var namespace_wpf_app =
[
    [ "Pages", "namespace_wpf_app_1_1_pages.html", "namespace_wpf_app_1_1_pages" ],
    [ "Properties", "namespace_wpf_app_1_1_properties.html", "namespace_wpf_app_1_1_properties" ],
    [ "App", "class_wpf_app_1_1_app.html", "class_wpf_app_1_1_app" ],
    [ "GameControl", "class_wpf_app_1_1_game_control.html", "class_wpf_app_1_1_game_control" ],
    [ "GameRenderer", "class_wpf_app_1_1_game_renderer.html", "class_wpf_app_1_1_game_renderer" ],
    [ "MainWindow", "class_wpf_app_1_1_main_window.html", "class_wpf_app_1_1_main_window" ],
    [ "MenuPage", "class_wpf_app_1_1_menu_page.html", "class_wpf_app_1_1_menu_page" ],
    [ "Navigator", "class_wpf_app_1_1_navigator.html", "class_wpf_app_1_1_navigator" ],
    [ "UserControl1", "class_wpf_app_1_1_user_control1.html", "class_wpf_app_1_1_user_control1" ]
];