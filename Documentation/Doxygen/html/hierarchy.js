var hierarchy =
[
    [ "Application", null, [
      [ "WpfApp.App", "class_wpf_app_1_1_app.html", null ]
    ] ],
    [ "ApplicationSettingsBase", null, [
      [ "WpfApp.Properties.Settings", "class_wpf_app_1_1_properties_1_1_settings.html", null ]
    ] ],
    [ "FrameworkElement", null, [
      [ "WpfApp.GameControl", "class_wpf_app_1_1_game_control.html", null ]
    ] ],
    [ "Model.GameItem", "class_model_1_1_game_item.html", [
      [ "Model.Item", "class_model_1_1_item.html", null ],
      [ "Model.Ladder", "class_model_1_1_ladder.html", null ],
      [ "Model.Unit", "class_model_1_1_unit.html", [
        [ "Model.Enemy", "class_model_1_1_enemy.html", null ],
        [ "Model.Player", "class_model_1_1_player.html", null ]
      ] ],
      [ "Model.Wall", "class_model_1_1_wall.html", null ]
    ] ],
    [ "WpfApp.GameRenderer", "class_wpf_app_1_1_game_renderer.html", null ],
    [ "IComponentConnector", null, [
      [ "WpfApp.MenuPage", "class_wpf_app_1_1_menu_page.html", null ],
      [ "WpfApp.Pages.EndgamePage", "class_wpf_app_1_1_pages_1_1_endgame_page.html", null ],
      [ "WpfApp.Pages.GamePage", "class_wpf_app_1_1_pages_1_1_game_page.html", null ],
      [ "WpfApp.Pages.LeadboardPage", "class_wpf_app_1_1_pages_1_1_leadboard_page.html", null ],
      [ "WpfApp.Pages.PausePage", "class_wpf_app_1_1_pages_1_1_pause_page.html", null ],
      [ "WpfApp.UserControl1", "class_wpf_app_1_1_user_control1.html", null ]
    ] ],
    [ "Logic.IGameLogic", "interface_logic_1_1_i_game_logic.html", [
      [ "Logic.GameLogic", "class_logic_1_1_game_logic.html", null ]
    ] ],
    [ "Model.IGameModel", "interface_model_1_1_i_game_model.html", [
      [ "Model.GameModel", "class_model_1_1_game_model.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Repository.LeadboardItem", "class_repository_1_1_leadboard_item.html", null ],
    [ "WpfApp.Navigator", "class_wpf_app_1_1_navigator.html", null ],
    [ "Page", null, [
      [ "WpfApp.MenuPage", "class_wpf_app_1_1_menu_page.html", null ],
      [ "WpfApp.Pages.EndgamePage", "class_wpf_app_1_1_pages_1_1_endgame_page.html", null ],
      [ "WpfApp.Pages.GamePage", "class_wpf_app_1_1_pages_1_1_game_page.html", null ],
      [ "WpfApp.Pages.LeadboardPage", "class_wpf_app_1_1_pages_1_1_leadboard_page.html", null ],
      [ "WpfApp.Pages.PausePage", "class_wpf_app_1_1_pages_1_1_pause_page.html", null ]
    ] ],
    [ "WpfApp.Properties.Resources", "class_wpf_app_1_1_properties_1_1_resources.html", null ],
    [ "Tests.UnitTests", "class_tests_1_1_unit_tests.html", null ],
    [ "UserControl", null, [
      [ "WpfApp.UserControl1", "class_wpf_app_1_1_user_control1.html", null ]
    ] ],
    [ "Window", null, [
      [ "WpfApp.MainWindow", "class_wpf_app_1_1_main_window.html", null ]
    ] ],
    [ "Repository.XML", "class_repository_1_1_x_m_l.html", null ]
];