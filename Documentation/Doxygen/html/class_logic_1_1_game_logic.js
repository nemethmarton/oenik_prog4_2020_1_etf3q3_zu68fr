var class_logic_1_1_game_logic =
[
    [ "GameLogic", "class_logic_1_1_game_logic.html#a538d8ba7f9fccf17bd14a912e92d618e", null ],
    [ "DecreaseFreezeTime", "class_logic_1_1_game_logic.html#a7a371eb89f1552d3faa3f41e23cfc44a", null ],
    [ "GetGameOver", "class_logic_1_1_game_logic.html#abac5229bc3e6b1581f68c9586cc36729", null ],
    [ "GetScore", "class_logic_1_1_game_logic.html#a3c090dde1abb63f8823063d4906be24b", null ],
    [ "MoveEnemy", "class_logic_1_1_game_logic.html#ad0eb8aa49576a28114374fbc9463d898", null ],
    [ "MovePlayer", "class_logic_1_1_game_logic.html#a1e08d7bb29ab43c842bdb30c919a6ef4", null ],
    [ "Pause", "class_logic_1_1_game_logic.html#abbbeedca0d5703e06f8ad5c00094db8f", null ],
    [ "PlayerGravity", "class_logic_1_1_game_logic.html#aecbdbb2a0b890ecf2d5d11e661a28652", null ],
    [ "SaveGame", "class_logic_1_1_game_logic.html#a1bd04ce165873c1a91f42d49525cc3b6", null ],
    [ "StageStart", "class_logic_1_1_game_logic.html#a7f3312883b0f0cd0b25daa75d77f8e7a", null ],
    [ "CurrentStageNum", "class_logic_1_1_game_logic.html#aa17b33c331f4ef4b909a6ad89aeb10ce", null ],
    [ "EnemiesFrozen", "class_logic_1_1_game_logic.html#ac29ab5ab54ec474e3cfada6734e70670", null ],
    [ "GameIsOver", "class_logic_1_1_game_logic.html#a52af41a2df7b3794dd23b25bcf765bb4", null ],
    [ "GameRunning", "class_logic_1_1_game_logic.html#aaf9eb1f545545a1d81cc231512cda731", null ],
    [ "RefreshScreen", "class_logic_1_1_game_logic.html#a1beef405243bfddf20e173f49f3ad7b8", null ]
];