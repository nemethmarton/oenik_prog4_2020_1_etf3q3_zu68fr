var interface_model_1_1_i_game_model =
[
    [ "EtcSetup", "interface_model_1_1_i_game_model.html#af8acbcd143da89a0c59a812b5fc6f95f", null ],
    [ "InitializeStage", "interface_model_1_1_i_game_model.html#a2a0fcfb52b86a3f9ec012a8f46d5c0ad", null ],
    [ "SaveGame", "interface_model_1_1_i_game_model.html#aa48045117125a0d3de30a0e406c77ba6", null ],
    [ "Enemies", "interface_model_1_1_i_game_model.html#ad25e640bb19f9c58bb4f517202e779db", null ],
    [ "GameHeight", "interface_model_1_1_i_game_model.html#a0d5a1081a0b10fb7c0690a1fe695cc06", null ],
    [ "GameWidth", "interface_model_1_1_i_game_model.html#ae92520a6cb01982bf6c7c27da89da042", null ],
    [ "Items", "interface_model_1_1_i_game_model.html#a6f6d67062db32d7c7b825f684e555c09", null ],
    [ "Ladders", "interface_model_1_1_i_game_model.html#ab4866dd26fda3bb2a21661d0b576d79a", null ],
    [ "Player", "interface_model_1_1_i_game_model.html#a1a5ffa019c5ec1bfdb57b5c4f3224c2d", null ],
    [ "Score", "interface_model_1_1_i_game_model.html#aab81ffaed7f69f6e880b424f692a6817", null ],
    [ "TileSize", "interface_model_1_1_i_game_model.html#a4f97dfbf770716e6968f725861c9133e", null ],
    [ "Walls", "interface_model_1_1_i_game_model.html#a87b1187da70bc948ecca42be67ced02a", null ]
];