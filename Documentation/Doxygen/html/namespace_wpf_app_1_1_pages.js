var namespace_wpf_app_1_1_pages =
[
    [ "EndgamePage", "class_wpf_app_1_1_pages_1_1_endgame_page.html", "class_wpf_app_1_1_pages_1_1_endgame_page" ],
    [ "GamePage", "class_wpf_app_1_1_pages_1_1_game_page.html", "class_wpf_app_1_1_pages_1_1_game_page" ],
    [ "LeadboardPage", "class_wpf_app_1_1_pages_1_1_leadboard_page.html", "class_wpf_app_1_1_pages_1_1_leadboard_page" ],
    [ "PausePage", "class_wpf_app_1_1_pages_1_1_pause_page.html", "class_wpf_app_1_1_pages_1_1_pause_page" ]
];