var indexSectionsWithContent =
{
  0: "abcdefghilmnoprstuwx",
  1: "aegilmnprsuwx",
  2: "lmrtwx",
  3: "abcdefgilmoprstuw",
  4: "aeglm",
  5: "d",
  6: "dlru",
  7: "acdeghilnprstw",
  8: "r",
  9: "cn"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "events",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Events",
  9: "Pages"
};

