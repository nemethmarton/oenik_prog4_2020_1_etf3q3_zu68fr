var searchData=
[
  ['pause_67',['Pause',['../class_logic_1_1_game_logic.html#abbbeedca0d5703e06f8ad5c00094db8f',1,'Logic.GameLogic.Pause()'],['../interface_logic_1_1_i_game_logic.html#a4738de8e5f9fb9bc776a9d509d2b485a',1,'Logic.IGameLogic.Pause()'],['../class_wpf_app_1_1_navigator.html#a9d548428ff243bdc8aaa0118d6e0bed7',1,'WpfApp.Navigator.Pause()']]],
  ['pausepage_68',['PausePage',['../class_wpf_app_1_1_pages_1_1_pause_page.html',1,'WpfApp.Pages.PausePage'],['../class_wpf_app_1_1_pages_1_1_pause_page.html#a82d23760a12011017622a3f3ba31f606',1,'WpfApp.Pages.PausePage.PausePage()']]],
  ['pausetest_69',['PauseTest',['../class_tests_1_1_unit_tests.html#a640452e0469b8899b728422b1b1b770f',1,'Tests::UnitTests']]],
  ['player_70',['Player',['../class_model_1_1_player.html',1,'Model.Player'],['../class_model_1_1_game_model.html#a1c05aa9516da21db5eee518a5d7036ed',1,'Model.GameModel.Player()'],['../interface_model_1_1_i_game_model.html#a1a5ffa019c5ec1bfdb57b5c4f3224c2d',1,'Model.IGameModel.Player()'],['../class_model_1_1_player.html#abba9b36d8872c92014ba1f102e1755cc',1,'Model.Player.Player()']]],
  ['playergravity_71',['PlayerGravity',['../class_logic_1_1_game_logic.html#aecbdbb2a0b890ecf2d5d11e661a28652',1,'Logic.GameLogic.PlayerGravity()'],['../interface_logic_1_1_i_game_logic.html#a083aed29eca4802971bab2e360a74067',1,'Logic.IGameLogic.PlayerGravity()']]],
  ['playergravitytest_72',['PlayerGravityTest',['../class_tests_1_1_unit_tests.html#a095f18730ae25658aa50e5957a76c978',1,'Tests::UnitTests']]]
];
