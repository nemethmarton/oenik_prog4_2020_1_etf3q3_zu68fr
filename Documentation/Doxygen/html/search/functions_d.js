var searchData=
[
  ['savegame_185',['SaveGame',['../class_logic_1_1_game_logic.html#a1bd04ce165873c1a91f42d49525cc3b6',1,'Logic.GameLogic.SaveGame()'],['../interface_logic_1_1_i_game_logic.html#a34f7539411b06b71e34dad520a45084c',1,'Logic.IGameLogic.SaveGame()'],['../class_model_1_1_game_model.html#a386ad752228c5ad1447b42cd6b71b4a0',1,'Model.GameModel.SaveGame()'],['../interface_model_1_1_i_game_model.html#aa48045117125a0d3de30a0e406c77ba6',1,'Model.IGameModel.SaveGame()'],['../class_repository_1_1_x_m_l.html#a51847b6e176fa76e787d332c16c5973e',1,'Repository.XML.SaveGame()']]],
  ['setpropertyvalue_186',['SetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#ade0f04c0f7b18dd5b170e071d5534d38',1,'XamlGeneratedNamespace::GeneratedInternalTypeHelper']]],
  ['setxy_187',['SetXY',['../class_model_1_1_unit.html#a47511104459f65f66df1e1cfe6a13d45',1,'Model::Unit']]],
  ['stagestart_188',['StageStart',['../class_logic_1_1_game_logic.html#a7f3312883b0f0cd0b25daa75d77f8e7a',1,'Logic.GameLogic.StageStart()'],['../interface_logic_1_1_i_game_logic.html#a09d2bb770edce2e69434ccec3f0e11ac',1,'Logic.IGameLogic.StageStart()']]],
  ['stagestarttest_189',['StageStartTest',['../class_tests_1_1_unit_tests.html#a249918d97d42b1a32b381023449c33d7',1,'Tests::UnitTests']]]
];
