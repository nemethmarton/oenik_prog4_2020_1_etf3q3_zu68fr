var searchData=
[
  ['pause_177',['Pause',['../class_logic_1_1_game_logic.html#abbbeedca0d5703e06f8ad5c00094db8f',1,'Logic.GameLogic.Pause()'],['../interface_logic_1_1_i_game_logic.html#a4738de8e5f9fb9bc776a9d509d2b485a',1,'Logic.IGameLogic.Pause()'],['../class_wpf_app_1_1_navigator.html#a9d548428ff243bdc8aaa0118d6e0bed7',1,'WpfApp.Navigator.Pause()']]],
  ['pausepage_178',['PausePage',['../class_wpf_app_1_1_pages_1_1_pause_page.html#a82d23760a12011017622a3f3ba31f606',1,'WpfApp::Pages::PausePage']]],
  ['pausetest_179',['PauseTest',['../class_tests_1_1_unit_tests.html#a640452e0469b8899b728422b1b1b770f',1,'Tests::UnitTests']]],
  ['player_180',['Player',['../class_model_1_1_player.html#abba9b36d8872c92014ba1f102e1755cc',1,'Model::Player']]],
  ['playergravity_181',['PlayerGravity',['../class_logic_1_1_game_logic.html#aecbdbb2a0b890ecf2d5d11e661a28652',1,'Logic.GameLogic.PlayerGravity()'],['../interface_logic_1_1_i_game_logic.html#a083aed29eca4802971bab2e360a74067',1,'Logic.IGameLogic.PlayerGravity()']]],
  ['playergravitytest_182',['PlayerGravityTest',['../class_tests_1_1_unit_tests.html#a095f18730ae25658aa50e5957a76c978',1,'Tests::UnitTests']]]
];
