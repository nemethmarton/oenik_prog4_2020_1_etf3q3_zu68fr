var searchData=
[
  ['tests_88',['Tests',['../namespace_tests.html',1,'']]],
  ['tilesize_89',['TileSize',['../class_model_1_1_game_model.html#a8e6a3efd1491ecfd75b48232e17965ab',1,'Model.GameModel.TileSize()'],['../interface_model_1_1_i_game_model.html#a4f97dfbf770716e6968f725861c9133e',1,'Model.IGameModel.TileSize()']]],
  ['tostring_90',['ToString',['../class_model_1_1_enemy.html#a3e37921f76ebb5a9e524927ccf0f8ec7',1,'Model.Enemy.ToString()'],['../class_model_1_1_item.html#a101e6e2339f4b80ea37e14c6479b1ee5',1,'Model.Item.ToString()'],['../class_model_1_1_ladder.html#ad9d2aa7ed58e5e46f4304d317b1aea74',1,'Model.Ladder.ToString()'],['../class_model_1_1_player.html#af000d134f3c00f9962483e0ceff54414',1,'Model.Player.ToString()'],['../class_model_1_1_wall.html#a804c27e870a78fbae9f8b92530145931',1,'Model.Wall.ToString()'],['../class_repository_1_1_leadboard_item.html#a62a33a2827545eb83b6cf2b2de48c793',1,'Repository.LeadboardItem.ToString()']]],
  ['type_91',['Type',['../class_model_1_1_enemy.html#aa5cb7728496744f75b552f75946a2e93',1,'Model.Enemy.Type()'],['../class_model_1_1_item.html#ac4f26a5483cfbf87c28c89d7b25d0e93',1,'Model.Item.Type()']]]
];
