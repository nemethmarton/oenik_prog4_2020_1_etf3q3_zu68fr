var class_model_1_1_unit =
[
    [ "Unit", "class_model_1_1_unit.html#a3cd9458c1ce78162d7f5fac92dda94b7", null ],
    [ "MoveX", "class_model_1_1_unit.html#a5afaabd19eb76e9bad3f9e53998fe7cb", null ],
    [ "MoveY", "class_model_1_1_unit.html#a5675e850a5e81a7876ec7de0ca4742a7", null ],
    [ "SetXY", "class_model_1_1_unit.html#a47511104459f65f66df1e1cfe6a13d45", null ],
    [ "Active", "class_model_1_1_unit.html#a90e6301e05898c556e66af40fe4d455c", null ],
    [ "Dx", "class_model_1_1_unit.html#aaeedc48dc8d8e614ad0b29d161d370c3", null ],
    [ "Dy", "class_model_1_1_unit.html#a1faca6c57ab66aac40268482ffe4420d", null ],
    [ "Speed", "class_model_1_1_unit.html#aecee4e038fc518751386c525331ac920", null ]
];