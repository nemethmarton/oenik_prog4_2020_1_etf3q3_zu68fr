var interface_logic_1_1_i_game_logic =
[
    [ "DecreaseFreezeTime", "interface_logic_1_1_i_game_logic.html#ad29cc12f63860077c1ad4e6166e9ff71", null ],
    [ "GetGameOver", "interface_logic_1_1_i_game_logic.html#a18c002703a4f7647302c6cfe4302f5f5", null ],
    [ "GetScore", "interface_logic_1_1_i_game_logic.html#a0e29602ae047b496519f4f236e8f5467", null ],
    [ "MoveEnemy", "interface_logic_1_1_i_game_logic.html#a0051fc39d42a5541dec2b27a802ea8a7", null ],
    [ "MovePlayer", "interface_logic_1_1_i_game_logic.html#a8274596bf39e72b567069bada7f438a3", null ],
    [ "Pause", "interface_logic_1_1_i_game_logic.html#a4738de8e5f9fb9bc776a9d509d2b485a", null ],
    [ "PlayerGravity", "interface_logic_1_1_i_game_logic.html#a083aed29eca4802971bab2e360a74067", null ],
    [ "SaveGame", "interface_logic_1_1_i_game_logic.html#a34f7539411b06b71e34dad520a45084c", null ],
    [ "StageStart", "interface_logic_1_1_i_game_logic.html#a09d2bb770edce2e69434ccec3f0e11ac", null ],
    [ "RefreshScreen", "interface_logic_1_1_i_game_logic.html#a5ce10efc5f4693c4e75c975683bcc6ba", null ]
];